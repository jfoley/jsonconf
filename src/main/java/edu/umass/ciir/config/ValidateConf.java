package edu.umass.ciir.config;

import edu.umass.ciir.config.json.JSONParser;
import java.io.IOException;

public class ValidateConf {
  public static void main(String[] args) throws IOException {
    for(String path : args) {
      System.out.println(JSONParser.fromFile(path));
    }
  }
}

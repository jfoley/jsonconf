
package edu.umass.ciir.config.impl;

import edu.umass.ciir.config.IConfig;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jfoley
 */
public class ConfigWithDefaults extends AConfig {
  public IConfig backoff = new Config();
  public IConfig data;
  
  public ConfigWithDefaults(IConfig primary) {
    data = primary;
  }
  public ConfigWithDefaults(IConfig primary, IConfig defs) {
    data = primary;
    backoff = defs;
  }
  
  public void setBackoff(AConfig b) {
    backoff = b;
  }
  
  /**
   * Collect the configuration from the backoff that this is actually using.
   * @return config object
   */
  public Config activeBackoff() {
    Config active = new Config();
    for(String key : backoff.keySet()) {
      if(data.containsKey(key)) continue;
      active.put(key, copyValue(get(key)));
    }
    return active;
  }

  @Override
  public int size() {
    return keySet().size();
  }

  @Override
  public boolean isEmpty() {
    return data.isEmpty() && backoff.isEmpty();
  }

  @Override
  public boolean containsKey(Object o) {
    return data.containsKey(o) || backoff.containsKey(o);
  }

  @Override
  public boolean containsValue(Object o) {
    return data.containsValue(o) || activeBackoff().containsValue(o);
  }

  @Override
  public Object get(Object o) {
    if(! (o instanceof String) )
      return null;
    String key = (String) o;
    
    if(data.containsKey(key))
      return data.get(key);
    else if(backoff.containsKey(key))
      return backoff.get(key);
    
    return null;
  }

  @Override
  public Object put(String k, Object v) {
    return data.put(k, v);
  }

  @Override
  public void putAll(Map<? extends String, ? extends Object> map) {
    data.putAll(map);
  }

  @Override
  public Set<String> keySet() {
    HashSet<String> uniqueKeys = new HashSet<String>();
    uniqueKeys.addAll(data.keySet());
    uniqueKeys.addAll(backoff.keySet());
    return uniqueKeys;
  }

  @Override
  public Collection<Object> values() {
    ArrayList<Object> vals = new ArrayList(data.values());
    vals.addAll(activeBackoff().values());
    return vals;
  }

  @Override
  public Set<Entry<String, Object>> entrySet() {
    HashSet<Entry<String,Object>> entries = new HashSet(data.entrySet());
    entries.addAll(activeBackoff().entrySet());
    return entries;
  }
  
}

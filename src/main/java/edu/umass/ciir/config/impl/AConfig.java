
package edu.umass.ciir.config.impl;

import edu.umass.ciir.config.IConfig;
import edu.umass.ciir.config.json.JSON;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jfoley
 */
abstract class AConfig implements IConfig {
  
  // things we don't support up first:
  
  @Override
  public Object remove(Object o) {
    throw new UnsupportedOperationException("We don't allow you to remove config things!");
  }
  
  @Override
  public void clear() {
    throw new UnsupportedOperationException("We don't allow you to clear config things! Just make a new one!");
  }
    
  @Override
  public int hashCode() {
    throw new UnsupportedOperationException("What would a good hashCode() for this be?");
  }
  
  
  public IConfig withDefaults(IConfig defaults) {
    return new ConfigWithDefaults(this, defaults);
  }
  public IConfig withBackoff(IConfig defaults) {
    return new ConfigWithDefaults(this, defaults);
  }
  
  
  /**
   * Copies a value, possibly recursively.
   * @param value
   * @return 
   */
  protected Object copyValue(Object value) {
    // copy a map inside this map
    if (value instanceof Config) {
      return (Object) ((Config) value).copy();
    } else if (value instanceof List) {
      // copy all elements of a list
      ArrayList cl = new ArrayList();
      for (Object o : ((List) value)) {
        cl.add(copyValue(o));
      }
      return cl;
    }
    
    return value;
  }
  
  /**
   * Does a deep copy of this configuration object.
   * Traverses lists and maps, doesn't copy leaf values (Long, Boolean, String).
   * @return 
   */
  @Override
  public IConfig copy() {
    Config clone = new Config();
    
    for(String key : keySet()) {
      clone.put(key, copyValue(get(key)));
    }
    
    return clone;
  }

  @Override
  public Object getOrThrow(String key) {
    if (!containsKey(key)) {
      throw new IllegalArgumentException("Key " + key + " does not exist!");
    }
    return get(key);
  }  

  @Override
  public List getList(String key) {
    Object val = getOrThrow(key);
    if (!(val instanceof List)) {
      throw new IllegalArgumentException("Key " + key + " is not a List in Config!");
    }
    return (List) val;
  }
  
  @Override
  public Config getMap(String key) {
    Object val = getOrThrow(key);
    if (!(val instanceof Config)) {
      throw new IllegalArgumentException("Key " + key + " is not a Map in Config!");
    }
    return (Config) val;
  }
  
  @Override
  public String getAsString(String key) {
    Object val = getOrThrow(key);
    if(val instanceof String)
      return (String) val;
    else if(val instanceof List || val instanceof Config)
      throw new IllegalArgumentException("Key "+key+" cannot be made into a string: is not a primitive!");
    
    return val.toString();
  }
    
  @Override
  public int getAsInt(String key) {
    Object val = getOrThrow(key);
    if(val instanceof String) {
      return Integer.parseInt((String) val);
    } else if(val instanceof Long) {
      return ((Long) val).intValue();
    } else if(val instanceof Boolean) {
      return ((Boolean) val).booleanValue() ? 1 : 0;
    } else if(val instanceof Double) {
      return (int) Math.floor(((Double) val).doubleValue());
    }
    throw new IllegalArgumentException("Key "+key+" cannot be made into an int!");
  }
  
  @Override
  public String getString(String key) {
    Object val = getOrThrow(key);
    if(val instanceof String || val == null)
      return (String) val;
    
    throw new IllegalArgumentException("Key "+key+" is not a string.");
  }
  
  @Override
  public long getLong(String key) {
    Object val = getOrThrow(key);
    if(val instanceof Long)
      return ((Long) val).longValue();
    
    throw new IllegalArgumentException("Key "+key+" is not a long.");
  }

  
  @Override
  public double getDouble(String key) {
    Object val = getOrThrow(key);
    if(val instanceof Double)
      return ((Double) val).doubleValue();
    
    throw new IllegalArgumentException("Key "+key+" is not a double.");
  }
  
  @Override
  public boolean getBoolean(String key) {
    Object val = getOrThrow(key);
    if(val instanceof Boolean) {
      return ((Boolean) val).booleanValue();
    }
    
    throw new IllegalArgumentException("Key "+key+" is not a boolean.");
  }
  
  /** Get the map for key if present or return orElse */
  @Override
  public Config get(String key, Config orElse) {
    if(!containsKey(key))
      return orElse;
    return getMap(key);
  }
  
  @Override
  public String get(String key, String orElse) {
    if(!containsKey(key))
      return orElse;
    return getString(key);
  }
  
  @Override
  public long get(String key, long orElse) {
    if(!containsKey(key))
      return orElse;
    return getLong(key);
  }
  
  @Override
  public int get(String key, int orElse) {
    return (int) get(key, (long) orElse);
  }
  
  @Override
  public double get(String key, double orElse) {
    if(!containsKey(key))
      return orElse;
    return getDouble(key);
  }
  
  @Override
  public boolean get(String key, boolean orElse) {
    if(!containsKey(key))
      return orElse;
    return getBoolean(key);
  }
  
  @Override
  public String toString() {
    return JSON.ofMap(this);
  }

  @Override
  public boolean isList(String key) {
    return containsKey(key) && getOrThrow(key) instanceof List;
  }

  @Override
  public boolean isMap(String key) {
    return containsKey(key) && getOrThrow(key) instanceof IConfig;
  }

  @Override
  public boolean isString(String key) {
    return containsKey(key) && ((getOrThrow(key) == null) ||(getOrThrow(key) instanceof String));
  }

  @Override
  public boolean isLong(String key) {
    return containsKey(key) && getOrThrow(key) instanceof Long;
  }

  @Override
  public boolean isDouble(String key) {
    return containsKey(key) && getOrThrow(key) instanceof Double;
  }

  @Override
  public boolean isBoolean(String key) {
    return containsKey(key) && getOrThrow(key) instanceof Boolean;
  }
}

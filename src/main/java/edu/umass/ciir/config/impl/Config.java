
package edu.umass.ciir.config.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jfoley
 */
public class Config extends AConfig {  
  private final HashMap<String, Object> data;
  
  public Config() {
    data = new HashMap<String,Object>();
  }
  
  @Override
  public int size() {
    return data.size();
  }

  @Override
  public boolean isEmpty() {
    return data.isEmpty();
  }

  @Override
  public boolean containsKey(Object o) {
    if(o instanceof String) {
      return data.containsKey(o);
    }
    return false;
  }

  @Override
  public boolean containsValue(Object o) {
    return data.containsValue(o);
  }

  @Override
  public Object get(Object o) {
    return data.get(o);
  }

  @Override
  public Object put(String k, Object v) {
    return data.put(k, v);
  }

  @Override
  public void putAll(Map<? extends String, ? extends Object> map) {
    data.putAll(map);
  }

  @Override
  public Set<String> keySet() {
    return data.keySet();
  }

  @Override
  public Collection<Object> values() {
    return data.values();
  }

  @Override
  public Set<Entry<String, Object>> entrySet() {
    return data.entrySet();
  }
  
  @Override
  public boolean equals(Object o) {
    if(!(o instanceof Config)) return false;
    if(o == this) return true;
    
    Config other = (Config) o;
    if(other.size() != this.size()) return false;
    
    for(String key : keySet()) {
      Object lhs = this.get(key);
      Object rhs = other.get(key);
      
      if(!lhs.equals(rhs)) return false;
    }
    
    return true;
  }


  
  
}

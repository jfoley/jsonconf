package edu.umass.ciir.config.json;

import edu.umass.ciir.config.IConfig;
import edu.umass.ciir.config.impl.Config;
import edu.umass.ciir.config.io.LineColPeekReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

/**
 *
 * @author jfoley
 */
public class JSONParser {  

  protected static IConfig parse(JSONParser jsonParser) {
    IConfig conf = jsonParser.parseMap();
    jsonParser.close();
    return conf;
  }

  public static IConfig fromFile(String path) throws IOException {
    return parse(new JSONParser(new BufferedReader(new FileReader(path)), path));
  }
  
  private final LineColPeekReader reader;
  protected JSONParser(Reader reader, String fileName) throws IOException {
    this.reader = new LineColPeekReader(reader, fileName);
  }
  
  public static IConfig fromString(String data) throws IOException {
    return parse(new JSONParser(new StringReader(data), "<string>"));
  }
  
  
  
  /** skip comments and spaces. */
  public void skip() {
    while(true) {
      int next = reader.peek();
      if(next == -1)
        return;
      char cx = (char) next;
      
      if(Character.isWhitespace(cx)) {
        reader.get();
        continue;
      }

      if(cx == '#') {
        reader.expect('#');
        reader.skipLine();
        continue;
      } else if(cx == '/') {
        reader.expect('/');
        reader.expect('/'); //TODO c-style multiline comments
        reader.skipLine();
        continue;
      }
      
      // don't skip whatever this character is
      return;
    }
  }

  public IConfig parseMap() {
    IConfig output = new Config();
    
    skip();
    reader.expect('{');
    
    while(true) {
      skip();
      char next = reader.peekc("map entries");
      if(next == '}') {
        reader.get();
        break;
      } else if(next == ',') {
        reader.get();
        continue;
      }
      parseKV(output);
    }
    
    return output;
  }

  public void parseKV(IConfig output) {
    StringBuilder ksb = new StringBuilder();
    
    String key = parseQuotedString();
    // find key-value splitter:
    skip();
    reader.expect(':');
    
    Object val = parseValue();    
    output.put(key, val); 
  }

  private Object parseValue() {
    skip();
    char first = reader.peekc("value");
    
    if (first == '"') {
      return parseQuotedString();
    } else if (first == '[') {
      return parseList();
    } else if (first == '{') {
      return parseMap();
    } else if (first == 't' || first == 'f') {
      return parseBool();
    } else if (first == 'n') {
      return parseNull();
    } else if (first == '-' || first == '.' || Character.isDigit(first)) {
      return parseNumber();
    } else {
      reader.error("Unexpected value starting with `" + first + "'");
      return null;
    }
  }

  private String parseQuotedString() {
    StringBuilder sb = new StringBuilder();
    reader.expect('"');
    
    while(true) {
      char next = reader.getc("string");
      if(next == '"') {
        break;
      }
      else if(next == '\\') {
        // handle escaped char
        char esc = reader.getc("string escape");
        switch(esc) {
          case '\\':
          case '"':
          case '/':
            sb.append(esc);
            break;
          case 'b': sb.append('\b'); break;
          case 'f': sb.append('\f'); break;
          case 't': sb.append('\t'); break;
          case 'r': sb.append('\r'); break;
          case 'n': sb.append('\n'); break;
          case 'u': sb.append(parseUnicode()); break;
        }
      } else {
        sb.append(next);
      }
    }
    
    return sb.toString();
  }

  // read in four hex digits
  private char parseUnicode() {
    String hex = "";
    for(int i=0; i<4; i++) {
      char ch = reader.getc("unicode literal hex char "+i);
      hex += ch;
    }
    int codePoint = Integer.parseInt(hex, 16);
    if(!Character.isValidCodePoint(codePoint)) {
      reader.error("Invalid code point specified in literal: "+codePoint);
    }
    
    return (char) codePoint;
  }

  private Object parseList() {
    ArrayList data = new ArrayList();
    reader.expect('[');
    
    while(true) {
      skip();
      char next = reader.peekc("list elements");
      if(next == ']') { // end of list
        reader.get();
        break;
      } else if(next == ',') { // next element
        reader.get();
        continue;
      }
      data.add(parseValue()); // element itself
    }
    
    return data;
  }

  private Object parseBool() {
    char first = reader.getc("boolean literal");
    assert(first == 'f' || first == 't');
    if(first == 'f') {
      reader.expect('a');
      reader.expect('l');
      reader.expect('s');
      reader.expect('e');
      return Boolean.FALSE;
    } else { //if(first == 't') {
      reader.expect('r');
      reader.expect('u');
      reader.expect('e');
      return Boolean.TRUE;
    }
  }

  private Double parseDouble(String str) {
    
    return null;
    
  }

  
  private Object parseNumber() {
    StringBuilder sb = new StringBuilder();
    while(true) {
      char ch = reader.peekc("number");
      if(Character.isLetterOrDigit(ch) || ch == '-' || ch == '.')
        sb.append(Character.toLowerCase(reader.getc()));
      else
        break;
    }
    String data = sb.toString();
    if(data.contains(".") || data.contains("e"))
      return Double.parseDouble(data);
    else
      return JSON.parseLong(data);
  }

  private Object parseNull() {
    reader.expect('n');
    reader.expect('u');
    reader.expect('l');
    reader.expect('l');
    return null;
  }

  private void close() {
    reader.close();
  }
}


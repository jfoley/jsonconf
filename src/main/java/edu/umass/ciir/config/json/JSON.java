package edu.umass.ciir.config.json;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jfoley
 */
public class JSON {
  
  public static long parseLong(String str) {
    int radix = 10;
    String data = str;
    
    if(str.startsWith("0x")) {
      radix = 16;
      data = str.substring(2);
    } else if(str.startsWith("0o")) {
      radix = 8; data = str.substring(2);
    } else if(str.startsWith("0b")) {
      radix = 2; data = str.substring(2);
    }

    return Long.parseLong(data, radix);
  }
  
  public static String escape(String value) {
    StringBuilder sb = new StringBuilder();
    sb.append('"');

    for (char ch : value.toCharArray()) {
      switch (ch) {
        case '\\':
          sb.append("\\\\");
          break;
        case '"':
          sb.append("\\\"");
          break;
        case '\b':
          sb.append("\\b");
          break;
        case '\f':
          sb.append("\\f");
          break;
        case '\t':
          sb.append("\\t");
          break;
        case '\r':
          sb.append("\\r");
          break;
        case '\n':
          sb.append("\\n");
          break;
        default:
          sb.append(ch);
      }
    }

    sb.append('"');
    return sb.toString();
  }

  public static String toString(Object obj) {
    if (obj instanceof Boolean) {
      return ((Boolean) obj) ? "true" : "false";
    } else if (obj instanceof Long) {
      return Long.toString((Long) obj);
    } else if (obj instanceof Integer) {
      return Integer.toString((Integer) obj);
    } else if (obj instanceof Double) {
      return Double.toString((Double) obj);
    } else if (obj instanceof String) {
      return escape((String) obj);
    } else if (obj instanceof List) {
      return ofList((List) obj);
    } else if (obj instanceof Map) {
      return ofMap((Map) obj);
    } else {
      return obj.toString();
    }
  }

  private static ArrayList<String> sortedStringKeys(Map map) {
    ArrayList oks = new ArrayList(map.keySet());
    ArrayList<String> keys = new ArrayList(oks.size());
    for (Object o : oks) {
      assert (o instanceof String);
      keys.add((String) o);
    }
    Collections.sort(keys);
    return keys;
  }

  public static String ofMap(Map map) {
    StringBuilder sb = new StringBuilder();
    sb.append("{");

    ArrayList<String> keys = sortedStringKeys(map);
    for (int i = 0; i < keys.size(); i++) {
      if (i > 0) {
        sb.append(", ");
      }
      String key = keys.get(i);
      sb.append(escape(key)).append(":");
      sb.append(toString(map.get(key)));
    }
    
    return sb.append("}").toString();
  }

  private static String ofList(List list) {
    StringBuilder sb = new StringBuilder();
    sb.append("[");

    for (int i = 0; i < list.size(); i++) {
      if (i > 0) {
        sb.append(", ");
      }
      sb.append(toString(list.get(i)));
    }

    return sb.append("]").toString();
  }
}

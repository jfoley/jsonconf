
package edu.umass.ciir.config;

import edu.umass.ciir.config.impl.Config;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jfoley
 */
public interface IConfig extends Map<String,Object>, Serializable {
  public IConfig copy();
  public Object getOrThrow(String key);
  
  /** Coerce the key into a string if possible. */
  public String getAsString(String key);
  /** Coerce the key into an int if a long. */
  public int getAsInt(String key);
  
  public boolean isList(String key);
  public boolean isMap(String key);
  public boolean isString(String key);
  public boolean isLong(String key);
  public boolean isDouble(String key);
  public boolean isBoolean(String key);
  
  public List getList(String key);
  public IConfig getMap(String key);
  public String getString(String key);
  public long getLong(String key);
  public double getDouble(String key);
  public boolean getBoolean(String key);
  
  public IConfig get(String key, Config orElse);
  public String get(String key, String orElse);
  public long get(String key, long orElse);
  public int get(String key, int orElse);
  public double get(String key, double orElse);
  public boolean get(String key, boolean orElse);
}

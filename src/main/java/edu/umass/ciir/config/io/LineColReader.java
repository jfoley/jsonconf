package edu.umass.ciir.config.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author jfoley
 */
public class LineColReader {
  private final BufferedReader reader;
  private final String sourceName;
  public int line = 1;
  public int col = 0;
  int current;
  
  public LineColReader(Reader source, String sourceName) throws IOException {
    this.reader = new BufferedReader(source);
    this.sourceName = sourceName;
    this.current = reader.read();
  }
  
  /**
   * Get the next character from the stream, or -1 if EOF.
   * Update internal line and col variables.
   * @return 
   * @throws java.io.IOException 
   */
  public int getc() throws IOException {    
    int prev = current;
    current = reader.read();
    
    if(prev == (int) '\n') {
      line++;
      col = 0;
    } else {
      col++;
    }
    return prev;
  }
  
  public int peek() {
    return current;
  }
  
  public void close() throws IOException {
    reader.close();
  }

  /** where string for errors */
  public String where() {
    return "F: "+sourceName+" L:"+line+" C:"+col+": ";
  }
}

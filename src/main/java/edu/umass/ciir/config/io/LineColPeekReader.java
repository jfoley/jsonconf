
package edu.umass.ciir.config.io;

import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author jfoley
 */
public class LineColPeekReader {
  private final LineColReader reader;
  public LineColPeekReader(Reader reader, String fileName) throws IOException {
    this.reader = new LineColReader(reader, fileName);
  }
  /**
   * When the next character is an EOF return -1.
   * @return 
   */
  public int get() {
    try {
      return reader.getc();
    } catch (IOException ex) {
      error(ex.getMessage());
    }
    return -1;
  }
  
  public int peek() {
    return reader.peek();
  }
  
  public void error(String msg) {
    throw new RuntimeException(reader.where()+msg);
  }
  
  /**
   * Get & cast the next character and complain on EOF.
   * @param what the thing we're trying to parse
   * @return the next character without advancing
   */
  public char peekc(String what) {
    int next = peek();
    if(next == -1)
      error("Unexpected EOF while parsing "+what);
    return (char) next;
  }
  
  /**
   * When we're sure it's not an EOF.
   * @return next character, advancing stream
   */
  public char getc() {
    int x = get();
    assert x != -1 : "next character is an EOF!";
    return (char) x;
  }
  
  /**
   * When we don't want it to be an EOF.
   * @param what
   * @return 
   */
  public char getc(String what) {
    int next = peek();
    if(next == -1)
      error("Unexpected EOF while parsing "+what);
    return getc();
  }
  
  /**
   * When we want the next character to be something specific.
   * @param next the next character needed
   */
  public void expect(char next) {
    if(peek() == -1) {
      error("Unexpected EOF, expected: `"+next+"'");
    }
    char ch = getc();
    if(next != ch) {
      error("Unexpected `"+ch+"', expected: `"+next+"'");
    }
  }
  
  /** eat a comment */
  public void skipLine() {
    while(true) {
      int next = get();
      if(next == -1 || next == (int) '\n')
        return;
    }
  }

  public void close() {
    try {
      reader.close();
    } catch (IOException ex) {
      error(ex.getMessage());
    }
  }
}

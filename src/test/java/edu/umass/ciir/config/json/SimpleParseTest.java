
package edu.umass.ciir.config.json;

import edu.umass.ciir.config.IConfig;
import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author jfoley
 */
public class SimpleParseTest {
  @Test
  public void testBools() throws IOException {
    String bools = "{\"a\" : true,\"b\":false}";
    IConfig conf = JSONParser.fromString(bools);
    
    assertTrue(conf.isBoolean("a"));
    assertTrue(conf.isBoolean("b"));
    assertTrue(conf.getBoolean("a"));
    assertFalse(conf.getBoolean("b"));
    
    IConfig conf2 = JSONParser.fromString(conf.toString());
    assertEquals(conf2, conf);
  }
  
  @Test
  public void testInts() throws IOException {
    String bools = "{\"a\" : 12,\"b\":-12}";
    IConfig conf = JSONParser.fromString(bools);
    
    assertTrue(conf.isLong("a"));
    assertTrue(conf.isLong("b"));
    assertEquals(12, conf.getLong("a"));
    assertEquals(-12, conf.getLong("b"));
    
    IConfig conf2 = JSONParser.fromString(conf.toString());
    assertEquals(conf2, conf);
  }
  
  @Test
  public void testDoubles() throws IOException {
    String dubs = "{\"foo\": 12.2e-3, \"bar\": -1.2}";
    IConfig conf = JSONParser.fromString(dubs);
    
    assertTrue(conf.isDouble("foo"));
    assertTrue(conf.isDouble("bar"));
    Assert.assertEquals(12.2e-3, conf.getDouble("foo"), 0.2);
    Assert.assertEquals(-1.2, conf.getDouble("bar"), 0.2);
  }
  
  @Test
  public void testComplex0() throws IOException {
    StringBuilder json = new StringBuilder();
    json.append("{ \"inner\" : { \"inner1\" : 1 , \"inner2\" : \"jackal\" } ");
    json.append(", \"key1\" : true ");
    json.append(", \"key2\" : false , \"key3\" : null ");
    json.append(", \"key4\" : [ 0 , 1 , 2 , 3 , 4 , 5 , 6 ] , \"key5\" : -4.56 }");


    IConfig p = JSONParser.fromString(json.toString());

    assertTrue(p.isBoolean("key1"));
    assertTrue(p.getBoolean("key1"));
    assertTrue(p.isBoolean("key2"));
    assertFalse(p.getBoolean("key2"));

    assertTrue(p.isString("key3"));
    assertTrue(p.getString("key3") == null);
    
    assertTrue(p.isList("key4"));
    List l = p.getList("key4");
    for (int i = 0; i < l.size(); i++) {
      assertEquals((long) i, ((Long) l.get(i)).longValue());
    }
  }
  
  @Test
  public void testTrailingCommas() throws Exception {
    IConfig test = JSONParser.fromString(" { \"foo\" : [1, 2,3,\t],\n}");
    assertTrue(test.isList("foo"));
    
    List l = test.getList("foo");
    assertTrue(l.size() == 3);
    for (int i = 0; i < l.size(); i++) {
      assertEquals((long) i+1, ((Long) l.get(i)).longValue());
    }
  }
}
